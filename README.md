# Announcement (30 June 2020) #

As of Mercurial support of Bitbucket finishes, the whole CRAFTY repositories have been moved to Github. 
Please find the new site at [https://github.com/CRAFTY-ABM](https://github.com/CRAFTY-ABM).

 